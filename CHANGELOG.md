## 1.3.1 (2021-07-23)


### Docs

* add README.md ([ff8140b](https://bitbucket.org/pulmer/vxbase-demo-node-module/commits/ff8140bf9c95ef93a9edf2b8686c30ce2bf65984))
* fix CHANGELOG.md ([536372a](https://bitbucket.org/pulmer/vxbase-demo-node-module/commits/536372a0e843597895254798094a03ce34283ab4))

### Fix

* add @semantic-release/git ([9c27a9d](https://bitbucket.org/pulmer/vxbase-demo-node-module/commits/9c27a9d1e919c8d6fc723bca3792b1ba1e1d22ae))
* add missing dependency @semantic-release/changelog ([d5572c4](https://bitbucket.org/pulmer/vxbase-demo-node-module/commits/d5572c43a120ebdf3dd257c4d83724d1cb077667))
* add more tests ([5b4a4f1](https://bitbucket.org/pulmer/vxbase-demo-node-module/commits/5b4a4f1cb1dac58e728a1c82f90421d1b8f94c7c))
* add unit tests ([a8bfb48](https://bitbucket.org/pulmer/vxbase-demo-node-module/commits/a8bfb48da189468f52d44aa872488964b3fca8c9))
* printMsg() output ([12a74c5](https://bitbucket.org/pulmer/vxbase-demo-node-module/commits/12a74c532540f845af3df4d3615c24697114f99f))
* typo ([f106454](https://bitbucket.org/pulmer/vxbase-demo-node-module/commits/f10645413ee0f403d6a0722bcd4a95bc065b9b94))

## 1.0.1 (2021-07-23)

### Fix

- add @semantic-release/git ([9c27a9d](https://bitbucket.org/pulmer/vxbase-demo-node-module/commits/9c27a9d1e919c8d6fc723bca3792b1ba1e1d22ae))

# 1.0.0 (2021-07-23)

### New

- add new method info 5cf87ae208179bfa11c1ddd2af97f65f975b6e43
- add msg argument e79763c023bdf5d2ea1157f01a3c976195e1a6cd

### Fix

- fix npm publish authentication 74b2999ef2823ad416dac56db0b1ea5d44f8517a
- update npm publish configuration fceec6b13cd9f4c8f256957ec4f40603136c9c1c
- test git pushing 47005cea1f892a0cc08b33a66819af48aa8b6135
- move semantic-release config eca56b34ee5daec6e06a23f8c1c25339858d27cd
- restrict publishing to master branch 2b347f634c88c68bda14e6da5f7f7227eaa43fb5

* add missing dependency 8ff2561e59fe7b829cc580cfa987ef8e4377de60

### Build

- remove compare link from changelog 2885dc6d0f6b56ed97267efedeafb7af2022d522
- replace release-it with semantic-release 9e2fa6063faf64dd3d93f5c659cbd3a0f756ea23
- write CHANGELOG.md on release f80f96cbbd54313724077c3328a313aade126cfd

### Update

- nop 9423c88ed9ad1609dfd5747680c4d8de2ef814e0
