const info = require('./index').info;
const printMsg = require('./index').printMsg;

test('console.log the text "Info: hello, world!"', () => {
    console.log = jest.fn();
    info('hello, world!');
    expect(console.log.mock.calls[0][0]).toBe('Info: hello, world!');
});

test('console.log the text "Message: hello, world!"', () => {
    console.log = jest.fn();
    printMsg('hello, world!');
    expect(console.log.mock.calls[0][0]).toBe('Message: hello, world!');
});
